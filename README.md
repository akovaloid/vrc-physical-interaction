# VRC Physical Interaction
  
Physical buttons for VRChat worlds that you can toggle with your hands.  
Made with UDON/UdonSharp for VRChat.  
(I hope to add more types of switches in the future)  
![](/docs/screenshot.jpg)  

## How to use
  
Download project and open in the Unity editor;  
Open provided sample scene;  
Build and Test in VRChat

## Requirements
  
You don't need to know C# for using these components.  
  
After downloading the project and opening it in Unity:  
- Install the latest VRCHAT SDK3 + UDON from:  https://vrchat.com/home/download  
- Install the latest UdonSharp from:  https://github.com/Merlin-san/UdonSharp  
  
Provided Button script can be replaced with your own (made with UDON graphs or U#)  
  
PushButton component supports two types of shapes: round and square  
(this is determined by the presence of a sphere or box collider on the component).  
It's better to use round buttons where possible.  
  
I recomend to Unpack Prefabs after adding them to your own scene,  
since I noticed some issues with UdonBehaviours inside prefabs.  
  
Please setup layers and layer collision matrix properly.  
![](/docs/layers.jpg)  
![](/docs/collision_matrix.jpg)  
(those two should collide only with each other)  